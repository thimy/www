+++
date = "2018-10-11T12:32:32+01:00"
title = "Parlons de notre nom"
author = "Tristram"
+++

> _Désolé pour notre nom. Venez quand même !_

Notre coopérative s’appelle _Codeurs en Liberté_. Ce nom ne laisse pas indifférent, en bien et en mal.

Comme souvent, trouver un nom a été difficile. Les amateurs d’archéologie trouveront sur [l’issue gitlab](https://gitlab.com/CodeursEnLiberte/fondations/issues/2) les réflexions et autres pistes qui ont amené à ce nom.

Nous aimons ce nom, mais il est problématique et nous souhaitons le changer. Comme nous n’arrivons pas à trouver un nouveau nom, nous essayons _a minima_ de nous expliquer.

## Pourquoi ce nom

Le nom porte plusieurs valeurs essentielles :

* Codeurs
    * chaque personne est importante et ne se dissout pas dans l’entreprise,
    * nous sommes des personnes qui faisons, pas un grade d’ingénieur dans une grosse structure.
* Liberté
    * nous n’avons pas de hiérarchie,
    * chaque personne choisit pour qui elle travaille,
    * chaque personne choisit combien de temps elle travaille,
    * nous incitons d’autres personnes à s’émanciper de la subordination.

Généralement le nom ne laisse pas indifférent et on nous sourit : il résonne chez les gens.

## Ce qui ne nous va plus

Dans un milieu encore trop masculin, _codeur_ est particulièrement peu adroit.

De plus il se rapporte à un métier très spécifique de l’informatique — alors que nous sommes ouverts à d’autres profils, dans le domaine du système, la conception graphique, conception d’interfaces, etc.

Enfin, dans une moindre mesure cela pourrait être considéré comme un angliscisme.

## Et maintenant ?

C’est une question qui revient régulièrement en interne. Nous avons fait plusieurs réflexions collectives sans aboutir à quelque chose qui nous plaise.

Peut-être qu’un jour nous trouverons un nom qui nous représente vraiment ?

Peut-être que nous choisirons plutôt un nom qui ne signifie rien ?

En attendant, nous ne pouvons qu’être conscient·e·s de ces problèmes, nous en excuser et espérer que certaines personnes souhaitant nous rejoindre ne se sentent plus exclues à cause du nom.
